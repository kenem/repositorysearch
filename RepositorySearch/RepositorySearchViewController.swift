//
//  ViewController.swift
//  RepositorySearch
//
//  Created by Ken Myers on 2018/11/19.
//

import UIKit

class RepositorySearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet var searchBarContainer: UIView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var errorLabel: UILabel!

    let searchController = UISearchController(searchResultsController: nil)
    var repoNames: [String]?
    var onRequestSuccess: ([String])->() = { _ in }
    var onRequestFail: (String)->() = { _ in }
    
    // Throttling
    // Text to search for once current call to findRepositoriesWithPrefix completes.
    var queuedSearchText: String?
    
    var searchInProgress: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchBar.placeholder = "Start typing a repository name..."
        searchController.searchBar.delegate = self
        
        self.tableView.tableHeaderView?.addSubview(searchController.searchBar)
        searchBarContainer.addSubview(searchController.searchBar)

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        onRequestSuccess = { items in
            DispatchQueue.main.async {
                self.tableView.isHidden = false
                self.errorLabel.isHidden = true
                self.repoNames = items
                self.tableView.reloadData()
                self.searchInProgress = false
                
                if let searchText = self.queuedSearchText {
                    self.updateTableView(searchText: searchText)
                    self.queuedSearchText = nil
                }
            }
        }
        onRequestFail = { errorText in
            DispatchQueue.main.async {
                self.errorLabel.text = errorText
                self.tableView.isHidden = true
                self.errorLabel.isHidden = false
                self.searchInProgress = false
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        searchController.searchBar.frame = searchBarContainer.bounds
    }
    
    // MARK: Table view datasource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let repoNames = repoNames {
            return repoNames.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        if let repoNames = repoNames {
            cell.textLabel?.text = repoNames[indexPath.row]
        } else {
            cell.textLabel?.text = ""
        }
        
        return cell

    }
    
    // MARK: Search bar delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        updateTableView(searchText: searchText)
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        updateTableView(searchText: "")
    }
    
    private func updateTableView(searchText: String) {
        if searchText.isEmpty {
            onRequestSuccess([])
        } else if searchInProgress {
            queuedSearchText = searchText
        } else {
            searchInProgress = true
            GitHubAPI.findRepositoriesWithPrefix(searchText,
                                                 onSuccess: onRequestSuccess,
                                                 onFailure: onRequestFail)
        }
    }

}

