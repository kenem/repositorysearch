//
//  AppDelegate.swift
//  RepositorySearch
//
//  Created by Ken Myers on 2018/11/19.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}
