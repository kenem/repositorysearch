//
//  GitHubAPI.swift
//  RepositorySearch
//
//  Created by Ken Myers on 2018/11/19.
//

import Foundation

class GitHubAPI {
    
    struct Repo: Decodable {
        var name: String
    }
    
    struct APIResponse: Decodable {
        var items: [Repo]
        
        enum CodingKeys: String, CodingKey {
            case items
        }
    }
    
    class func findRepositoriesWithPrefix(_ prefix: String,
                                          onSuccess: @escaping ([String])->(),
                                          onFailure: @escaping (String)->()) {
        let urlString = "https://api.github.com/search/repositories?q=\(prefix)"
        guard let url = URL(string: urlString) else {
            onFailure("Invalid search string.")
            return
        }
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: url) { (data, urlResponse, error) in
            guard let data = data else {
                if let errorDescription = error?.localizedDescription {
                    onFailure(errorDescription)
                } else {
                    onFailure("Unknown error")
                }
                return
            }

            do {
                let result = try JSONDecoder().decode(APIResponse.self, from: data)
                let names = result.items.map({ $0.name })
                onSuccess(names)
            } catch {
                
            }
        }
        task.resume()
    }
    
}
